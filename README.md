# OWASP Damn Vulnerable Web Application

Damn Vulnerable Web Application (DVWA) é uma aplicação web PHP/MySQL que é extremamente vulnerável. Seu principal objetivo é auxiliar os profissionais de segurança a testar suas habilidades e ferramentas em um ambiente legal, ajudar os desenvolvedores da Web a entender melhor os processos de proteção de aplicativos da Web e ajudar alunos e professores a aprender sobre segurança de aplicativos da Web em uma aula controlada ambiente da sala.

O objetivo do DVWA é praticar algumas das vulnerabilidades web mais comuns, com vários níveis de dificuldade, com uma interface simples e direta. Observe que existem vulnerabilidades documentadas e não documentadas com este software. Isso é intencional. Você é encorajado a tentar descobrir o maior número possível de problemas.

**AVISO** Esta imagem é vulnerável a vários tipos de ataques, por favor, não a implante em nenhum servidor público.

Projeto Original: [https://github.com/digininja/DVWA](https://github.com/digininja/DVWA)

Este projeto é um fork de: [https://github.com/opsxcq/docker-vulnerable-dvwa/blob/master/Dockerfile](https://github.com/opsxcq/docker-vulnerable-dvwa/blob/master/Dockerfile)
## Executando a imagem

Primeiro, execute o build da imagem:

```shell
docker build -t dvwa:latest .    
```

Execute a imagem com o comando:
```shell
docker container run -d --name dvwa -p 8087:80 dvwadocker
```

E espere até baixar a imagem e iniciá-la, depois disso você poderá ver a imagem rodando na sua máquina local:

![configuração](https://github.com/opsxcq/docker-vulnerable-dvwa/blob/master/setup.png?raw=tru)

Basta clicar no botão ```Create / Reset database``` e ele irá gerar qualquer configuração adicional necessária.

## Faça login com credenciais padrão

Para fazer o login, você pode usar as seguintes credenciais:

  * User: admin
  * Pass: password

## Defina o nível de dificuldade

O nível de dificuldade padrão é o nível ```impossível```, você pode alterá-lo no item ```DVWA Security``` no menu à esquerda.

![dificuldade](https://github.com/opsxcq/docker-vulnerable-dvwa/blob/master/setup-dificulty.png?raw=tru)

## Hackeie e divirta-se!

Se você estiver jogando com pouca dificuldade, apenas para ter um gostinho de como explorar uma falha neste aplicativo, vá para ```SQL Injection``` no menu à esquerda.
No campo id, adicione esta consulta:

%' e 1=0 union selecione null, concat(user,':',password) from users #

![sqli](https://github.com/opsxcq/docker-vulnerable-dvwa/blob/master/sqli.png?raw=true)

Existem várias outras maneiras e outras vulnerabilidades de explorar, vá em frente, divirta-se!

## Sobre a DVWA

Você pode visitar DVWA [site oficial](http://www.dvwa.co.uk/) e oficial [github repository](https://github.com/ethicalhack3r/DVWA) se quiser mais informações.

## Isenção de responsabilidade

Este ou o programa anterior é SOMENTE para fins educacionais. Não use sem permissão. A isenção de responsabilidade usual se aplica, especialmente o fato de que me (opsxcq) não é responsável por quaisquer danos causados ​​pelo uso direto ou indireto das informações ou funcionalidades fornecidas por esses programas. O autor ou qualquer provedor de Internet NÃO se responsabiliza pelo conteúdo ou uso indevido desses programas ou de quaisquer derivados deles. Ao usar esses programas, você aceita o fato de que qualquer dano (perda de dados, travamento do sistema, comprometimento do sistema, etc.) causado pelo uso desses programas não é de responsabilidade da opsxcq.
